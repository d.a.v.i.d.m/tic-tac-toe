


"use strict";

// only global variable; the actual tic-tac-toe board
var tableCells;


var Player = function(name, symbol) {


    this.setSymbol = function(letter) {

        // minor, probably unneeded error checking
        if (/X|O/.test(letter)) {
            this.symbol = letter;
        }
    }
}


var Settings = function(players, turns, dimensions, curTurn, over) {

    // resets settings only relevant to previous game
    this.newGame = function() {

        this.turns = 0;
        this.over = false;
    };

    // true = p1, false = p2
    this.setTurn = function(bool) {

        this.curTurn = bool;
    };

    this.getTurn = function() {
        return this.curTurn;
    };
}



var settings = new Settings();



var player1 = new Player("player1");
var player2 = new Player("player2");







// Sets event listeners that records / asks for setting for the game. Runs immediately
(function addOptionEvents() {


    var playerInfo = document.getElementById("playerInfo");
    var opt = document.getElementsByClassName("opt");
    var h3 = document.getElementsByTagName("h3")[0];
    var fadeOut = document.getElementsByClassName("fadeOut");



    settings.turns = 0;

    playerInfo.innerHTML += "Players: ";


    for (var i = 0; i < opt.length; i++) {

        opt[i].addEventListener("click", function() {

            // the and conditions prevent the user from clicking on something twice to stop it from copying their
            // choice down multiple times in playerInfo top bar
            if (/1|2/.test(this.innerHTML) && !/1|2/.test(playerInfo.innerHTML)) {

                userNumberOpt(this, fadeOut, playerInfo);
            }


            else if (/X|O/.test(this.innerHTML) && !/X|O/.test(playerInfo.innerHTML)) {

                userSymbolOpt(this, fadeOut, playerInfo);
            }

            else if (/3x3|5x5/.test(this.innerHTML) && !/3|5/.test(playerInfo.innerHTML)) {

                userDimensionsOpt(this, fadeOut, playerInfo);
            }

            // new game or play again with same settings
            else if (this.innerHTML === "Go" && document.getElementById("options").style.opacity !== "0"
                      || /Yes/.test(this.innerHTML)) {

                document.getElementsByTagName("table")[0].style.opacity = "1";

                tableCells = go();

                playerInfo.innerHTML = "";

                settings.setTurn(true);

                addTableEvents();
            }


            else if (this.innerHTML === "Play Again") {

                settings.newGame();

                transFade(fadeOut, "Same Settings?", "Yes", "No");
            }


            else if (/No/.test(this.innerHTML)) {

                playerInfo.innerHTML = "";

                startNewOpt(playerInfo, fadeOut);
            }

        });
    }

}());



/**********
The opt function group puts the user's settings on the top of page so can change it if needed
then loads the next setting's options
***********/

// used if multiple games. If first game, hard coded into html
function startNewOpt(playerInfo, fadeOut) {

    createContent("span", "  Players: ", playerInfo);

    transFade(fadeOut, "Players:", "1", "2");
}


function userNumberOpt(thisButton, fadeOut, playerInfo) {

    settings.players = Number(thisButton.innerHTML);

    createContent("span", settings.players, playerInfo);

    createContent("span", "  Player 1: ", playerInfo);

    transFade(fadeOut, "Symbol:", "X", "O");
}



function userSymbolOpt(thisButton, fadeOut, playerInfo) {

    player1.setSymbol(thisButton.innerHTML);
    player2.setSymbol((player1.symbol === "X") ? "O" : "X");

    createContent("span", player1.symbol, playerInfo);

    transFade(fadeOut, "Dimensions", "3x3", "5x5");

    createContent("span", " Dimensions: ", playerInfo);


    for (var i = 0; i < fadeOut.length; i++) {
        fadeOut[i].style.opacity = "0";
    }
}



function userDimensionsOpt(thisButton, fadeOut, playerInfo) {

    settings.dimensions = Number(thisButton.innerHTML[0]);


    createContent("span", thisButton.innerHTML, playerInfo);

    transFade(fadeOut, "Ready?", "Go");


    for (var i = 0; i < fadeOut.length; i++) {
        fadeOut[i].style.opacity = "0";
    }

}

function go() {

    document.getElementById("options").style.display = "none";

    return init();
}


function createContent(type, text, father, tag) {

    var child  = document.createElement(type);

    child.innerHTML = text;
    father.appendChild(child);

    changeAtr(child);
}


// previous settings can be changed by clicking on it from the top row
function changeAtr(element) {

    element.onclick = function() {


          if (player1.symbol === this.innerHTML) {

              [player1.symbol, player2.symbol] = [player2.symbol, player1.symbol];
              this.innerHTML = player1.symbol;
          }

          else if (this.innerHTML == settings.players) {

              settings.players = (settings.players == 1) ? 2 : 1;
              this.innerHTML = settings.players;
          }

          else if (this.innerHTML === (settings.dimensions + "x" + settings.dimensions)) {

              settings.dimensions = (settings.dimensions === 3) ? 5 : 3;

              this.innerHTML = (settings.dimensions + "x" + settings.dimensions);
          }
    }

}

// makes options heading and buttons fade away, changes them, then fade back in
function transFade(elements, h3Text, button1Text, button2Text) {


    for (var i = 0; i < elements.length; i++) {
        elements[i].style.opacity = "0";
    }

    setTimeout(function() {

        elements[0].innerHTML = h3Text;
        elements[1].innerHTML = button1Text;
        elements[2].innerHTML = button2Text;


        var number = (button2Text === undefined) ? elements.length - 1 : elements.length;

        for (var i = 0; i < number; i++) {
            elements[i].style.opacity = "1";
        }

    }, 2000);
}




// creates the board
// sets up 2d array to keep track of things, wiring to the cell that corresponds to place in array
function init() {

    var table = document.getElementsByTagName("table")[0];

    var tableArr = [];


    for (var i = 0; i < settings.dimensions; i++) {

        var row = table.insertRow(i);

        tableArr.push([]);


        for (var j = 0; j < settings.dimensions; j++) {

            var cell = row.insertCell(j);


            removeBorder(cell, i, j);

            tableArr[i][j] = cell;
        }
    }

    table.style.display = "block";

    return tableArr;
}


// takes away outer borders so it looks more like tic-tac-toe and less like a table
function removeBorder(cell, i, j) {

    if (i === 0) {
        cell.style.borderTop = "none";
    }

    if (i === settings.dimensions - 1) {
        cell.style.borderBottom = "none";
    }

    if (j === 0) {
        cell.style.borderLeft = "none";
    }

    if (j === settings.dimensions - 1) {
        cell.style.borderRight = "none";
    }

}









// when click on a table cell, move function is invoked
function addTableEvents() {

    for (var i = 0; i < tableCells.length; i++) {

        for (var j = 0; j < tableCells[i].length; j++) {


            tableCells[i][j].addEventListener("click", function() {

                  move(this);
            });
        }

    }
}



function AI(opponent, computer) {


    var rows = [], emptyArr = [];


    if (settings.dimensions % 2 !== 0 && settings.turns === 1) {

        var mid = (tableCells.length - 1) / 2;


        // prevent famous double trap and other traps that rely on middle
        if (tableCells[mid][mid].innerHTML === "") {

              return move(tableCells[mid][mid]);
        }

    }


    // check rows
    let [rows1, emptyArr1] = test(opponent, computer, true, false);

    // check cols
    let [rows2, emptyArr2] = test(opponent, computer, false, false);




    rows = rows1.concat(rows2);
    emptyArr = emptyArr1.concat(emptyArr2);



    for (var i = 0; i < rows.length; i++) {

        // if one more move will win the game for horizontal or vertical for the AI
        if (rows[i][0] === tableCells.length - 1 && rows[i][2] === 1) {

            return move(emptyArr[i]);
        }
    }




    var holder = 0;
    var indMost;


    // gets the index of row or col where opponent has most chance of winning
    for (var i = 0; i < rows.length; i++) {


        if (rows[i][1] > holder && emptyArr[i] !== undefined) {

            holder = rows[i][1];
            indMost = i;
        }
    }


    // if opponent will win in the next turn row or col, block
    if (holder === tableCells.length - 1) {

        return move(emptyArr[indMost]);
    }


    let [slantLR, slantEmpty] = test(opponent, computer, false, true);

    // if opponent or AI can win next turn slant l to r, go there
    if ((slantLR[0] === tableCells.length - 1 || slantLR[1] === tableCells.length - 1) && slantLR[2] === 1) {

        let empty = slantEmpty.find(function(element) {
            return element !== undefined;
        });

        return move(empty);
    }



    let [opp, comp, emptyRLslant] = slantRtoL(tableCells, opponent, computer);

    // 1 more move to win (either opponent or ai), take the spot
    if ((opp === tableCells.length - 1 || comp === tableCells.length - 1) && emptyRLslant !== 0) {

        return move(emptyRLslant);
    }



    /* If get to here, means a win is NOT going to happen in the text turn
    (unless it's some sort of double trap) so move anywhere */

    // get a random number based on the date
    var random = new Date().getTime().toString();


    random = random.substring(random.length - 2);


    while (emptyArr.indexOf(undefined) !== -1) {
        emptyArr.splice(emptyArr.indexOf(undefined), 1);
    }

    return move(emptyArr[random % emptyArr.length]);
}







// records how many places per row, col, or r-l slant the opponent or the computer has. Also records an empty spot in each row, col, or slant if there is one in case want to move there
function test(opponent, computer, horizontal, slant) {



    var rows = [], emptyArr = [];

    var compCount = 0, opponCount = 0, emptyCount = 0;

  // rows (going left to right)
  tableCells.forEach(function(outer, outInd) {

    // if it is a slant, don't want to reset since only 3 values anyways
    // if it's not, then want to record how many spots are comp, opponents, or empty per row/col
    if (!slant) {
        compCount = 0, opponCount = 0, emptyCount = 0;
    }

      // initialize each place to undefined
      rows.push(undefined);
      emptyArr.push(undefined);


      for (var inInd = 0; inInd < outer.length; inInd++) {

          // rows VS cols VS l-r slant
          var inner = (horizontal) ? outer[inInd] : (slant) ? tableCells[outInd][outInd] : tableCells[inInd][outInd];


          // checks where opponent is
          if (inner.innerHTML === opponent.symbol) {

              opponCount++;
          }


          else if (inner.innerHTML === "" && emptyArr[emptyArr.length - 1] === undefined) {

              emptyArr[emptyArr.length - 1] = inner;
              emptyCount++;
          }

          // checks if already blocking the opponent
          else if (inner.innerHTML === computer.symbol) {

              compCount++;
          }


          if (slant) {
              break;
          }

      }

      // doesn't do anything for slants. Don't know if better style to just leave it in or add an if statement since no difference
      rows[outInd] = [compCount, opponCount, emptyCount];



  });


    if (!slant) {
        return [rows, emptyArr];
    }

    // for slants, there's only 1 value per iteration so doesn't make sense to make separate array for each thing
    return [[compCount, opponCount, emptyCount], emptyArr];
}



// upper right to down left. Would clutter up the above function that does this for rows, cols, and l-r slant if added this to it
function slantRtoL(opponent, computer) {

    var comp = 0, opp = 0, emptyRLslant = 0;

    for (var i = tableCells.length - 1, j = 0; i > -1; i--, j++) {

        if (tableCells[i][j].innerHTML === opponent.symbol) {

            opp++;
        }

        else if (tableCells[i][j].innerHTML === "" && emptyRLslant == 0) {

            emptyRLslant = tableCells[i][j];

        }

        else if (tableCells[i][j].innerHTML === computer.symbol) {
            comp++;
        }
    }

    return [opp, comp, emptyRLslant];
}



function move(cell) {

    var playerInfo = document.getElementById("playerInfo");

    // if game over, freeze
    if (settings.over) {

        return;
    }


    if (cell.innerHTML !== "") {
        return;
    }


    var symbol = cell.innerHTML = (settings.getTurn()) ? player1.symbol : player2.symbol;



    if (cell.innerHTML === "X") {
          cell.style.color = "red";
    }


    var state = checkState(player1, player2);

    if (state) {

        settings.over = true;
      //  return winStage(state);
    }


    // switch turns
    settings.setTurn(!settings.getTurn());

    settings.turns++;

    playerInfo.innerHTML = (settings.getTurn()) ? "Player 1 (" + player1.symbol + ")" : "Player 2 (" + player2.symbol + ")";


    // if 1 player game, AI is released
    if (settings.players === 1 && !settings.getTurn()) {

        return AI(tableCells, player1, player2);
    }

}



// checks for win or tie
function checkState(player1, player2) {

    var horizontal = true;
    var slant = false;

    // check row
    let [rows] = test(player1, player2, horizontal, slant);


    horizontal = false;

    // col
    let [cols] = test(player1, player2, horizontal, slant);


    slant = true;

    // slants
    let [lToR] = test(player1, player2, horizontal, slant);

    let rToL = slantRtoL(tableCells, player1, player2);


    rows = rows.concat(cols).concat([lToR]).concat([rToL]);


    for (var i = 0; i < rows.length; i++) {

        if (rows[i].indexOf(settings.dimensions) === 0 || rows[i].indexOf(settings.dimensions) === 1) {
            return true;
        }
    }


    var tie = rows.every(function(element) {
        return element[2] === 0;
    });


    if (tie) {
        return "tie";
    }

    return false;

}






function winStage(status) {

    var winner = (settings.getTurn()) ? "Player1" : "Player2";


    var playerInfo = document.getElementById("playerInfo");
    var table = document.getElementsByTagName("table")[0];
    var options = document.getElementById("options");
    var fadeOut = document.getElementsByClassName("fadeOut");



    playerInfo.innerHTML = "";


    options.style.display = "flex";


    table.style.opacity = "0";

    options.style.opacity = "0";

    options.style.position = "absolute";


    setTimeout(function() {

        table.style.display = "none";

        options.style.position = "relative";

        options.style.opacity = "1";


        table.removeChild(document.getElementsByTagName("tbody")[0]);

        var tbody = document.createElement("tbody");

        table.appendChild(tbody);
    }, 2000);


    var message = (status === "tie") ? "Tie Game" : "Congrats " + winner;

    transFade(fadeOut, message, "Play Again");


}
